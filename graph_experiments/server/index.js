"use strict";
const express = require('express')
    , Session = require('./lib/session.js')
    , app = express()
    , fs = require('fs');

app.use(express.static(__dirname + '/../dist'));

let to_read = process.argv[2];
console.log(`Reading data from ${to_read}`);

const samples_for = function(session_name) {
  return `${to_read}/${session_name}/samples.json`
}

app.get('/sessions', function (req, res) {
    return fs.readdir(to_read, function(err, dirs) {
        let found_sessions = [];
        if (!err)
        {
            dirs.forEach((dir) => {
                try {
                  fs.accessSync(samples_for(dir), fs.F_OK);
                  found_sessions.push(new Session(dir));
                } catch (e) {
                    console.warn(`Ignoring ${dir} - missing samples`);
                }
            });
        }

        let sessions = {
            sessions: found_sessions
        };

        res.send(JSON.stringify(sessions));
    });

});

app.get('/session/*', function (req, res) {
    const session_name = req.params[0];

    const relative_path = samples_for(session_name);

    fs.readFile(relative_path, 'utf8', function(err, contents) {
        if (err)
        {
            console.error(`Unable to open file ${relative_path}`);
            res.status(500).send('Unable to find session data!');
            return;
        }

        console.error(`Sending file data ${relative_path}`);
        const graph_data = JSON.parse('[' + contents + ']');

        let session = {
            title: session_name,
            source: "XXX",
            dataset: graph_data,
            graphs: [
                {
                    title: "TEST4",
                    data: graph_data,
                    type: 'stack',
                    yformat: '%',
                },
                {
                    title: "TEST5",
                    data: graph_data,
                    type: 'line',
                    yformat: ".2f",
                },
            ],
        };
        res.send(JSON.stringify(session));
    });
});

app.use('/source/', express.static(__dirname + '/../web/'));

const port = process.env.PORT || 3000;
console.log(`Listening on ${port}`);
app.listen(port);
