
app.service("Chart", function() {

  return class Chart
  {
    constructor(container, width, height, x_axis, y_axis)
    {
      this.objects = [];
      this.container = container;
      this.width = width;
      this.height = height;
      this.x_axis = x_axis;
      this.y_axis = y_axis;
    }

    graph_root()
    {
      return this.root_svg;
    }

    add_graph_clip(obj)
    {
      obj.attr("clip-path", "url(#graph-clip)");
    }

    add_object(object)
    {
      this.objects.push(object);

      if (this.x_axis.auto_scale)
      {
        this.x_axis.extend(object.x_range());
      }

      if (this.y_axis.auto_scale)
      {
        this.y_axis.extend(object.y_range());
      }
    }

    build()
    {
      const margin = {top: 20, right: 20, bottom: 30, left: 50};
      this.graph_width = this.width - margin.left - margin.right,
      this.graph_height = this.height - margin.top - margin.bottom;

      this.x = d3.scaleTime()
          .range([0, this.graph_width]);
      this.x.domain(this.x_axis.range);

      this.y = d3.scaleLinear()
          .range([this.graph_height, 0]);
      this.y.domain(this.y_axis.range);

      const xAxis = d3.axisBottom()
          .scale(this.x)
          .tickFormat(this.x_axis.format);

      const yAxis = d3.axisLeft()
          .scale(this.y)
          .tickFormat(this.y_axis._format);

      this.container.selectAll("*").remove();
      this.root_svg = this.container.append("svg")
          .attr("width", this.graph_width + margin.left + margin.right)
          .attr("height", this.graph_height + margin.top + margin.bottom)
        .append("g")
          .attr("transform", "translate(" + margin.left + "," + margin.top + ")");

      this.root_svg.append("clipPath")
            .attr("id", "graph-clip")
          .append("rect")
            .attr("x", this.x.range()[0])
            .attr("y", this.y.range()[1])
            .attr("width", this.x.range()[1])
            .attr("height", this.y.range()[0]);

      this.root_svg
        .append("rect")
          .attr("x", this.x.range()[0])
          .attr("y", this.y.range()[1])
          .attr("width", this.x.range()[1])
          .attr("height", this.y.range()[0])
          .style('fill', 'white');

      this.root_svg.append("g")
          .attr("class", "x axis")
          .attr("transform", "translate(0," + this.graph_height + ")")
          .call(xAxis);

      this.root_svg.append("g")
          .attr("class", "y axis")
          .call(yAxis);

      // Now build all child objects into this
      this.objects.forEach((obj) => {
          try {
              obj.build(this)
          }
          catch(e) {
              console.error(`Error building chart component ${obj}`);
              throw e;
          }

      });
    }
  }
});
