
app.service("Axis", function() {

  return class Axis
  {
    constructor(auto_scale, range, format)
    {
      this.auto_scale = auto_scale;
      this.range = range;
      this.format = format;
    }

    extend(extents)
    {
      this.range[0] = Math.min(extents[0], this.range[0]);
      this.range[1] = Math.max(extents[1], this.range[1]);
    }
  }
});
