
let flatten_object = function(out, object, prefix = "") {
    for (const i in object) {
        const name = prefix + i;
        const val = object[i];
        if ($.type(val) === "object")
        {
            flatten_object(out, val, name + ".");
        }
        else
        {
            out[name] = val;
        }
    }
    return out;
};

let make_relative = function(items, keys) {
    let previous = {};

    for (var row_i in items) {
        var row = items[row_i];

        if (!$.isEmptyObject(previous)) {
            keys.forEach((k) => row[k] = row[k] - previous[k]);
        }

        $.extend(previous, row)
    }
}

class Process
{
    constructor()
    {
        this._row_processors = [];
        this._rows_processors = [];
    }

    copy()
    {
        const p = new Process();
        p._row_processors = $.extend([], this._row_processors);
        p._rows_processors = $.extend([], this._rows_processors);
        return p;
    }

    process(dataset)
    {
        const rows_out = [];
        for (var row_i in dataset.rows) {
            const row = dataset.rows[row_i];
            let out_row = $.extend({}, row);
            this._row_processors.forEach((r) => out_row = r(out_row));
            rows_out.push(out_row);
        }

        this._rows_processors.forEach((r) => r(rows_out));

        return new DataSet(rows_out);
    }

    make_relative(keys)
    {
        return this.transform_rows((rows) => {
            make_relative(rows, keys);
        });
    }

    flatten()
    {
        return this.transform_row((row) => {
            let out_row = {};
            flatten_object(out_row, row);
            return out_row;
        });
    }

    transform_rows(tx)
    {
        let c = this.copy();
        c._rows_processors.push(tx);
        return c;
    }

    transform_row(tx)
    {
        let c = this.copy();
        c._row_processors.push(tx);
        return c;
    }

    select_row(selector)
    {
        return this.transform_row((row) => {
            let row_out = selector.process(row);
            return row_out;
        });
    }

    transform_field(field, tx)
    {
        let c = this.copy();
        c._row_processors.push((row) => {
            row[field] = tx(row[field]);
            return row;
        });
        return c;
    }

    inject_field(field, create)
    {
        let c = this.copy();
        c._row_processors.push((row) => {
            row[field] = create(row);
            return row;
        });
        return c;
    }
}

class DataSet
{
    constructor(data)
    {
        this._rows = data;
    }

    get rows()
    {
        return this._rows;
    }

    transform(process)
    {
        return new DataSet(process(this._rows));
    }
}

class GraphSampler
{
    constructor(graph, key)
    {
        this.graph = graph;
        this.key = key;
    }

    get data()
    {
        return this.graph.dataset.rows;
    }

    x(row)
    {
        return row.date;
    }

    y(row)
    {
        const val = row[this.key];
        if (!val)
        {
            return 0;
        }
        return val;
    }

    values()
    {
        return this.data.map((data) => {
            return { x: this.x(data), y: this.y(data) };
        });
    }
}

class Graph
{
    constructor(title, type, subtype, xformat, yformat, process, dataset, data_items)
    {
        this._title = title;
        this._type = type;
        this._subtype = subtype;
        this._xformat = xformat;
        this._yformat = yformat;
        this._process = process;
        this._dataset = this._process.process(dataset);
        this._data_items = data_items
    }

    get title()
    {
        return this._title;
    }

    get type()
    {
        return this._type;
    }

    get subtype()
    {
        return this._subtype;
    }

    get x_format()
    {
        if (this._xformat == null)
        {
            return null;
        }
        return d3.format(this._xformat);
    }

    get y_format()
    {
        if (this._yformat == null)
        {
            return null;
        }
        return d3.format(this._yformat);
    }

    get dataset()
    {
        return this._dataset;
    }

    get x_axis_key()
    {
    }

    get data_items()
    {
        return this._data_items;
    }

    sampler_for(name)
    {
        return new GraphSampler(this, name);
    }
}

class Selector
{
    constructor()
    {
        this.fields = [];
    }

    add(source, dest)
    {
        if (typeof source === "string")
        {
            source = new FieldSelector(source);
        }
        else if (source instanceof Array)
        {
            source = new FieldSelector(...source);
        }
        if (typeof dest === "string")
        {
            dest = new FieldEmitter(dest);
        }
        else if (dest instanceof Function)
        {
            dest = new FieldEmitter(dest);
        }
        this.fields.push({ 'source': source, 'destination': dest});
        return this;
    }

    process(row)
    {
        let row_out = {};
        this.fields.forEach((field) => {
            const src = field.source;
            const dst = field.destination;

            let matches = src.match(row);
            matches.forEach((m) => {
                dst.emit(row, m, row_out);
            });
        });

        return row_out;
    }
}

class FieldSelector
{
    constructor(...args)
    {
        this.selection = args
    }

    match(row)
    {
        return this.match_from(row, 0, []);
    }

    match_from(data, index, prefix)
    {
        const level = this.selection[index];

        let matches = [];
        if (typeof level === "string")
        {
            if (data[level] != undefined)
            {
                matches.push(level);
            }
        }
        else
        {
            for (const k in data)
            {
                let match = k.match(level);
                if (match)
                {
                    matches.push(k);
                }
            }
        }

        const new_prefix = prefix.concat(level);
        if (index === (this.selection.length-1))
        {
            return matches.map((e) => prefix.concat(e));
        }

        let flattened_matches = [];
        matches.forEach((e) => {
            const new_matches = this.match_from(data[e], index+1, prefix.concat(e));
            flattened_matches = flattened_matches.concat(new_matches);
        });
        return flattened_matches;
    }
}

class FieldEmitter
{
    constructor(...args)
    {
        this.emission = args
    }

    emit(row, match_data, output)
    {
        this.emit_from(row, match_data, output, 0);
    }

    emit_from(row, match_data, output, index)
    {
        const level = this.emission[index];

        let level_key = null;
        if (typeof level === "string")
        {
            level_key = level;
        }
        else
        {
            level_key = level(match_data);
        }

        if (index === (this.emission.length-1))
        {
            let value = row;
            match_data.forEach((k) => {
                value = value[k];
            });
            output[level_key] = value;
            return;
        }

        let next_level = output[level_key];
        if (next_level == undefined)
        {
            next_level = output[level_key] = {}
        }
        this.emit_from(row, match_data, next_level, index + 1);
    }
}

class Session
{
    constructor(data)
    {
        this._data = data;
        this._dataset = new DataSet(data.dataset);

        const now = new Date();

        let cpu_process = new Process()
            .inject_field("date", (row) => new Date(now.getTime() + row.__time__ * 1000))
            .select_row(new Selector()
                .add("date", "date")
                .add([ "process", /.*/, "cpu" ], (m) => `${m[1]}`)
            );

        let memory_pvi_process = new Process()
            .inject_field("date", (row) => new Date(now.getTime() + row.__time__ * 1000))
            .select_row(new Selector()
                .add("date", "date")
                .add([ "process", /.*/, "pvi" ], (m) => `${m[1]}`)
            );

        let memory_wst_process = new Process()
            .inject_field("date", (row) => new Date(now.getTime() + row.__time__ * 1000))
            .select_row(new Selector()
                .add("date", "date")
                .add([ "process", /.*/, "wst" ], (m) => `${m[1]}`)
            );


        let experiment_process = new Process()
            .inject_field("date", (row) => new Date(now.getTime() + row.__time__ * 1000))
            .select_row(new Selector()
                .add("date", "date")
                .add([ "yield", /.*/, "acquisition", "raw_per_channel" ], (m) => `${m[1]}_aquisition_`)
                .add([ "yield", /.*/, "analyser", "raw_per_channel" ], (m) => `${m[1]}_analysis`)
            );

        let basecalling_process = new Process()
            .inject_field("date", (row) => new Date(now.getTime() + row.__time__ * 1000))
            .select_row(new Selector()
                .add("date", "date")
                .add([ "yield", /.*/, "analyser", "basecaller", /reads_.*/ ], (m) => `${m[1]}_${m[4]}`)
                .add([ "yield", /.*/, "analyser", "basecaller", "pending_reads" ], (m) => `${m[1]}_${m[4]}`)
            );


        const colours = d3.scaleOrdinal(d3.schemeCategory10);

        const build_data_items = function(process, dataset) {
            let data_items = {};
            const processed = process.process(dataset);
            let index = 0;
            for (let k in processed.rows[Math.floor(processed.rows.length/2)])
            {
                if (k == "date")
                {
                    continue;
                }

                data_items[k] = {
                    colour: colours(index)
                }
                ++index;
            }
            return data_items;
        };

        const process_data_items = build_data_items(cpu_process, this._dataset);
        const experiment_data_items = build_data_items(experiment_process, this._dataset);
        const basecalling_data_items = build_data_items(basecalling_process, this._dataset);


        this._graphs = [
            new Graph("CPU", "graph", "stack", null, "%", cpu_process, this._dataset, process_data_items),
            new Graph("Memory Private", "graph", "stack", null, "%", memory_pvi_process, this._dataset, process_data_items),
            new Graph("Memory Working Set", "graph", "stack", null, "%", memory_wst_process, this._dataset, process_data_items),
            new Graph("Experiment", "graph", "line", null, "%", experiment_process, this._dataset, experiment_data_items),
            new Graph("Basecalling", "graph", "line", null, "%", basecalling_process, this._dataset, basecalling_data_items),
            new Graph("All", "text", "", null, "", new Process(), this._dataset, process_data_items),
            //new Graph("CPU Percent", "stack", null, "%", process, this._dataset, data_items),
            //new Graph("TEST5", "line", null, ".2f", process.make_relative(Object.keys(data_items)), this._dataset, data_items)
        ];
    }

    get title()
    {
        return this._data.title;
    }

    get source()
    {
        return this._data.source;
    }

    get dataset()
    {
        return this._dataset;
    }

    get graphs()
    {
        return this._graphs;
    }

    get extents()
    {
        let ranges = [];
        this.graphs.forEach((graph) => {
            let g_range = d3.extent(graph.dataset.rows, function(d) { return d.date; });
            ranges.push(g_range);
        });
        return [
            d3.min(ranges, (r) => r[0]),
            d3.min(ranges, (r) => r[1]),
        ];
    }
}

app.factory('sessions', function($http) {
    return {
        sessions: function() {
            return $http({
                method: 'GET',
                url: '/sessions'
            }).then(function successCallback(response) {
                let out = {};
                response.data.sessions.forEach((e) =>
                {
                    out[e.name] = e;
                });
                return out;
            }, function errorCallback(_) {
                return {};
            });
        },

        session: function(name) {
            return $http({
                method: 'GET',
                url: `/session/${name}`
            }).then(function successCallback(response) {
                return new Session(response.data);
            }, function errorCallback(_) {
                return {};
            });
        }
    };
});
