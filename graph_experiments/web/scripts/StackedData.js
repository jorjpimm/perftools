
app.service("StackedData", function() {

    return class StackedData
    {
        constructor(id, entries)
        {
            this.id = id;
            this.entries = entries;

            let stack = d3.stack()
                .keys(Object.keys(this.entries))
                .order(d3.stackOrderDescending)
                .value(function(d, key) {
                    const sampler = entries[key].sampler;
                    return sampler.y(d);
                });

            this.x_sampler = null;
            for (const k in this.entries)
            {
                this.x_sampler = this.entries[k].sampler;
                break;
            }

            if (this.x_sampler)
            {
                this.stack = stack(this.x_sampler.data);
            }
        }

        x_range()
        {
            if (!this.stack)
            {
                return [0, 0];
            }
            
            const x_sampler = this.x_sampler;

            let min = d3.min(this.x_sampler.data, d => {
                return x_sampler.x(d);
            });

            let max = d3.max(this.x_sampler.data, d => {
                return x_sampler.x(d);
            });

            return [min, max];
        }

        y_range()
        {
            if (!this.stack)
            {
                return [0, 0];
            }
            
            let min = d3.min(this.stack, d => {
                return d3.max([0, d3.min(d, val => val[0])]);
            });

            let max = d3.max(this.stack, d => {
                return d3.max(d, val => val[1]);
            });

            return [min, max];
        }

        build(chart)
        {
            if (this.stack)
            {
                const entries = this.entries;
                const x_sampler = this.x_sampler;
                var area = d3.area()
                    .x(function(d, i) {
                        return chart.x(x_sampler.x(d.data));
                    })
                    .y0(function(d) {
                        return chart.y(d[0]);
                    })
                    .y1(function(d) {
                        return chart.y(d[1]);
                    });

                let stacked_g = chart.graph_root().selectAll("." + this.id)
                        .data(this.stack)
                    .enter().append("g")
                        .attr("class", this.id);

                let path = stacked_g.append("path")
                    .attr("class", "area")
                    .attr("d", function(d) {
                        return area(d);
                    })
                    .style("fill", function(d, i) {
                        let entry = entries[i];
                        return entry.colour;
                    });
                chart.add_graph_clip(path);
            }
        }
    }
});
