/* jshint -W097 */
"use strict";

const app = angular.module("graphexp", [
  'ngRoute',
  'ngAnimate',
]);

app.controller("mainController", function($scope, $rootScope, $location, $timeout, sessions) {
  $scope.graphs = [];
  $scope.view_range = [];

  $scope.sessions = [];
  sessions.sessions().then(function(sess)
  {
    $scope.sessions = sess;
  });

  $scope.sessions = [];
  sessions.sessions().then(function(sess)
  {
    $scope.sessions = sess;
  });

  $scope.set_session = function(session_name)
  {
    sessions.session(session_name).then((session) =>
    {
      $scope.session = session;

      $scope.selector_hidden = true;


      let reset_view_range = function()
      {
        let extent = session.extents;
        $scope.max_view_range = {};
        $scope.max_view_range.min = extent[0];
        $scope.max_view_range.max = extent[1];
        $scope.view_range = {};
        $scope.view_range.min = extent[0];
        $scope.view_range.max = extent[1];
      }

      reset_view_range();

      $timeout(function() {
        $scope.$apply();
      });
    });
  };

  $rootScope.$on('$locationChangeSuccess', function () {
    const session_name = $location.path().slice(1);
    $scope.set_session(session_name);
  });

  $scope.set_session($location.path().slice(1));
  $scope.selector_hidden = true;
});
