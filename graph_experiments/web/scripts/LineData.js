
app.service("LineData", function() {
  "use strict";

  return class LineData
  {
    constructor(id, entries)
    {
      this.id = id;
      this.entries = entries;
    }

    x_range()
    {
      let max = d3.max(this.entries, d => {
        return d3.max(d.sampler.data, val => d.sampler.x(val));
      });

      let min = d3.min(this.entries, d => {
        return d3.min(d.sampler.data, val => d.sampler.x(val));
      });
      return [ min, max ];
    }

    y_range()
    {
      let max = d3.max(this.entries, d => {
        return d3.max(d.sampler.data, val => d.sampler.y(val));
      });

      let min = d3.min(this.entries, d => {
        return d3.min(d.sampler.data, val => d.sampler.y(val));
      });
      return [ min, max ];
    }

    build(chart)
    {
      let line_g = chart.graph_root().selectAll("." + this.id)
          .data(this.entries)
        .enter().append("g")
          .attr("class", this.id);

      let path = line_g.append("path")
          .attr("class", "area")
          .attr("d", function(entry_data) {
              let line = d3.line()
                  .x(function(d) {
                      return chart.x(entry_data.sampler.x(d));
                  })
                  .y(function(d) {
                      return chart.y(entry_data.sampler.y(d));
                  })
              return line(entry_data.sampler.data);
          })
          .style("stroke", function(d) { return d.colour; })
          .style("stroke-width", 2)
          .style('fill', 'transparent');
      chart.add_graph_clip(path);
    }
  }
});
