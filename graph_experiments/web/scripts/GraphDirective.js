
app.directive('graph', function($timeout, Axis, Chart, StackedData, LineData, MouseInteraction) {
  return {
    templateUrl: 'partials/GraphDirective.html',
    scope: {
      title: '=title',
      data: '=content',
      xrange: '=xrange',
      xrangemax: '=xrangemax',
    },
    link: function($scope, $element, _$attrs) {
      const graph = $scope.data;
      const type = $scope.data.type;

      let data = graph.dataset.rows;
      $scope.keys = {};
      $scope.expanded = false;

      $scope.change_x_range = function(x1, x2) {
        $scope.xrange.min = Math.min(x1, x2);
        $scope.xrange.max = Math.max(x1, x2);

        $timeout(function() {
          $scope.$apply();
        });
      }

      $scope.reset_x = function() {
        $scope.change_x_range($scope.xrangemax.min, $scope.xrangemax.max);
      };

      const keys = graph.data_items;
      for (let i in keys) {
        $scope.keys[i] = {
          show: true,
          colour: keys[i].colour,
        };
      }

      const entries = {
        domain: function() {
          return d3.keys($scope.keys).filter(function(key) { return $scope.keys[key].show === true; });
        },
        colour: function(i) {
          return $scope.keys[i].colour;
        },
      };

      const container = d3.select($element[0]).select('#svg_container');

      const draw = function() {
        let mapped_entries = entries.domain().map(function(name) {
          return {
            name: name,
            sampler: graph.sampler_for(name),
            colour: entries.colour(name),
          };
        });

        let chart = new Chart(
          container,
          1035,
          500,
          new Axis(
            false,
            [ $scope.xrange.min, $scope.xrange.max ],
            $scope.data.x_format
          ),
          new Axis(
            true,
            [ Infinity, -Infinity ],
            $scope.data.y_format
          )
        );

        const type = $scope.data.subtype;
        if (type == 'stack')
        {
          chart.add_object(new StackedData('browser', mapped_entries));
        }
        else if (type == 'line')
        {
          chart.add_object(new LineData('browser', mapped_entries));
        }

        chart.add_object(new MouseInteraction({
          change_x_range: $scope.change_x_range,
        }));
        chart.build();
      };

      $scope.$watch("keys", draw, true);
      $scope.$watch("xrange", draw, true);
    },
  };
});
